# Medusa 3.3V PIR Motion Sensor

![Medusa 3.3V PIR Motion Sensor](https://shop.edwinrobotics.com/4149-thickbox_default/medusa-33-v-pir-motion-sensor.jpg "Medusa 3.3V PIR Motion Sensor")

[Medusa 3.3V PIR Motion Sensor](https://shop.edwinrobotics.com/modules/1238-medusa-33-v-pir-motion-sensor.html)

The low cost and widely common HC-SR501 PIR sensor with a now with a Medusa Connector and 3.3V system compatible. 

Specifications:

Operating Voltage: 3.3-5V

Sensing Distance: within 7 meters


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.